import Vue from 'vue'
import VueRouter from 'vue-router'
require('./bootstrap');
import Pageheader from './components/Pageheader.vue'
import routes from "./routes";
import Swal from 'sweetalert2'
window.Swal= Swal
const router = new VueRouter({
    mode: 'history',
    routes // short for `routes: routes`
})

Vue.use(VueRouter)
const app = new Vue({
    el: "#app",
    router,
    components: { Pageheader }
});
