<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\PostRequest;
use App\Models\post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function CreatePost(PostRequest $request)
    {
        post::create($request->all());

    }
}
